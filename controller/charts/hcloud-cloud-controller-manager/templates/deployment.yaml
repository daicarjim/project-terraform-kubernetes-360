apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "cloudControllerResourceBaseName" . }}
  labels:
    {{- include "cloudControllerDefaultLabels" . | nindent 4 }}
    {{- if .Values.manager.deployment.labels }}
    {{ toYaml .Values.manager.deployment.labels | indent 4 }}
    {{- end }}
  annotations:
    {{- if .Values.manager.deployment.annotations }}
    {{ toYaml .Values.manager.deployment.annotations | indent 4 }}
    {{- end }}
spec:
  replicas: 1
  revisionHistoryLimit: 2
  selector:
    matchLabels:
      {{- include "cloudControllerDefaultMatchLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "cloudControllerDefaultLabels" . | nindent 8 }}
        {{- if .Values.manager.deployment.template.labels }}
        {{ toYaml .Values.manager.deployment.template.labels | indent 4 }}
        {{- end }}
      annotations:
        scheduler.alpha.kubernetes.io/critical-pod: ""
        {{- if .Values.manager.deployment.template.annotations }}
        {{ toYaml .Values.manager.deployment.template.annotations | indent 4 }}
        {{- end }}
    spec:
      serviceAccountName: {{ include "cloudControllerResourceBaseName" . }}
      dnsPolicy: Default
      tolerations:
        # this taint is set by all kubelets running `--cloud-provider=external`
        # so we should tolerate it to schedule the cloud controller manager
        - key: "node.cloudprovider.kubernetes.io/uninitialized"
          value: "true"
          effect: "NoSchedule"
        - key: "CriticalAddonsOnly"
          operator: "Exists"
        # cloud controller manages should be able to run on masters
        - key: "node-role.kubernetes.io/master"
          effect: NoSchedule
        - key: "node.kubernetes.io/not-ready"
          effect: "NoSchedule"
{{- if .Values.manager.privateNetwork.enabled }}
      hostNetwork: true
{{- end }}
      containers:
        - image: {{ .Values.manager.deployment.image }}
          imagePullPolicy: {{ .Values.manager.deployment.imagePullPolicy }}
          name: {{ include "cloudControllerResourceBaseName" . }}
          command:
            - "/bin/hcloud-cloud-controller-manager"
            - "--cloud-provider=hcloud"
            - "--leader-elect=false"
            - "--allow-untagged-cloud"
{{- if .Values.manager.privateNetwork.enabled }}
            - "--allocate-node-cidrs=true"
            - "--cluster-cidr={{ .Values.manager.privateNetwork.clusterSubnet }}"
{{- end }}
          resources:
            requests:
              cpu: 100m
              memory: 50Mi
          env:
            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: HCLOUD_TOKEN
              valueFrom:
                secretKeyRef:
                  name: {{ include "cloudControllerManagerSecretName" . }}
                  key: token
            - name: HCLOUD_DEBUG
              value: "{{ .Values.manager.deployment.debug }}"
            - name: HCLOUD_LOAD_BALANCERS_ENABLED
              value: "{{ .Values.manager.loadBalancers.enabled }}"
{{- if .Values.manager.privateNetwork.enabled }}
            - name: HCLOUD_NETWORK
              value: "{{ .Values.manager.privateNetwork.id }}"
{{- end }}
