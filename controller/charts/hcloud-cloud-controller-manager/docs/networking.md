# Network Plugin Configuration

## Types of Kubernetes-internal Networking

Regarding networking, Kubernetes clusters on Hetzner cloud can be operated in the following different setups:
  * **Tunneled routing using public IP/main network interface**:
    The network plugin will create an overlay network, which is only available within the Kubernetes cluster.
    Packets sent between two nodes will get wrapped and sent using the default network interface.
    The target node will unwrap the received oacket and deliver them using the encapsulated destination IP.
    Reply packets will be sent to the encapsulated source IP, wrapped again, sent to the public IP of the source node,
    where the packet will be unwrapped again.
    Depending on the encapsulation technique used (e.g. IPIP, vxlan), this will add some overhead data to every packet
    sent to another node and therefore decrease throughput and increase latency.
    This configuration should basically work on any node.
  * **Tunneled routing using private IP/additional cloud network interface**:
    Similar to the configuration above, all packets sent between different nodes are wrapped and routed using an overlay network.
    The only difference is that packets between nodes are delivered using a private network interface.
    If the private network provides a better connection between nodes compared to the public IP (e.g. direct internal routing in a data center),
    this can increase performance.
    Additionally, since the packets are not sent through any public networks, this is also an improvement regarding data protection and security.
    This configuration works if a private network connection between all nodes is available.
    Despite the advantage of a (possibly) faster interconnection, the overhead per packet remains.
  * **Native routing using private IP/additional cloud network interface**:
    In this configuration also a private network is required.
    The network plugin does not create an overlay network, but uses parts of the subnets assigned to the network for Kubernetes (pods and services).
    Compared to the configurations before, in this case the router of the private network must be aware about the allocated IP blocks
    of the Kubernetes pod subnet of the nodes and route the particular IP blocks to the correct node.
    Any change of allocation must be immediately reflected by the network's router.
    This solution only works where a cloud environment specific controller can access IP allocation information of the Kubernetes cluster and accordingly configure the network's router.
    Usually, this only works in managed environments, e.g. Hetzner cloud, where hcloud-cloud-controller-manager does the job,
    or where a L2 network is used to connect the nodes.
    In this case, the overhead per packet does not occur anymore, since internal Kubernetes traffic can use the nodes' native network, including the routing.


## Configuring Network Plugins

Typically, the default settings for a network plugin are set in a way which directly supports configuration type 1 or 2.
From the network plugin's point of view, there is no difference if a private subnet or a public network is used for inter-node communication.
To support the third configuration type, usually plugins have to be configured to disable packet encapsulation.


### calico

In order to use calico for native routing, you have to disable package encapsulation
(see https://docs.projectcalico.org/reference/node/configuration):

```
CALICO_IPV4POOL_IPIP=Never
CALICO_IPV4POOL_VXLAN=Never
```

### cilium

Cilium offers an option, to disable tunneling and therefore enables native routing of packets
(see https://docs.cilium.io/en/v1.8/concepts/networking/routing/#native-routing).
Cilium also needs some more configuration:

```yaml
tunnel: disabled                     # enable native routing mode

blacklist-conflicting-routes: false  # disable blacklisting of IPs which collide with a
                                     # local route (e.g. network route of hcloud network)

native-routing-cidr: x.x.x.x/y       # CIDR where native routing is supported

auto-direct-node-routes: false       # Only enable if all nodes are connected using an L2 network (hcloud networks are using L3).
                                     # With this option, it is possible to have native routing without support of the network's router.
```


### flannel

To disable packet encapsulation in flannel, you have to use the experimental backend type "Alloc".
(see https://github.com/coreos/flannel/blob/master/Documentation/backends.md#alloc).

```json
{
  # [...]
  "Backend": {
    "Type": "alloc"
  }
}
```

Using this, flannel will allocate IP subnets used by nodes for pod IP assignment, but does not care about routing.
When using hcloud-cloud-controller-manager, it will create a route which points to the right node when flannel allocates a new subnet.
