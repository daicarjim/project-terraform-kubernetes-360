# Changelog

## v1.0.4

  * Upgraded to hcloud-csi-driver v1.5.1 (#3)
  * Introduced configuration variables `csiDriver.image` and `csiDriver.imagePullPolicy`

## v1.0.3

  * Upgraded to hcloud-csi-driver v1.5.0 (#2)

## v1.0.2

  * Fixed naming interference with hcloud-cloud-controller-manager helm chart (#1)

## v1.0.1

  * Added logo/icon

## v1.0.0

  * First release
